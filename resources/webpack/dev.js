const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const config = require('./common.js')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const browser = require('browser-sync').create('watch')
// browser-sync start --server --files **/*.css, **/*.html, **/*.js, **/*.vue

browser.init({
    ui: false,
    files: [path.join(process.env.RESOURCES_PATH, '**/*.html'), path.join(process.env.PUBLIC_PATH, '**', '*.*')],
    proxy: '127.0.0.1',
    prot: 8080,
    online: false,
    open: false
})

module.exports = merge(config, {
    watchOptions: {
        poll: 1000,
        aggregateTimeout: 200,
        ignored: /node_modules/
    },
    resolve: {
        alias: {
            'vue': 'vue/dist/vue.js'
        }
    },
    plugins: [
        new CleanWebpackPlugin(['lib'], {
            root: process.env.PUBLIC_PATH,
            verbose: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'lib/vendor'
        }),
    ]
})
