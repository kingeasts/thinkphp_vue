const path = require('path')
const glob = require('glob')
const ExtractCss = require('extract-text-webpack-plugin')
const libs = ['vue', 'axios']

let maps = []

const entries = function () {
    const dir = path.resolve(process.env.RESOURCES_PATH, 'assets')
    const files = glob.sync(dir + '/**/*.js')
    let map = {
        "lib/vendor": libs
    }
    for (let i = 0; i < files.length; i++) {
        let filepath = files[i]
        const _i = filepath.lastIndexOf('.')
        let paths = filepath.substring(0, _i).split('/').splice(-2)
        let filename = 'lib/' + paths.join('/')
        map[filename] = filepath
    }
    return map
}

module.exports = {
    cache: true,
    devtool: process.env.PRED ? 'eval-source-map' : 'source-map',
    entry: entries(),
    // devtool: "in"
    output: {
        path: process.env.PUBLIC_PATH,
        filename: '[name].js',
        sourceMapFilename: '[name].map'
    },
    resolve: {  //导入的时候不用写拓展名
        extensions: [' ', '.js', '.vue', '.scss', '.css']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                loader: 'vue-loader'
            },
		    {
			  test: /\.css$/,
			  use: ['style-loader', 'css-loader']
		    },
		    {
			  test: /\.(ttf|eot|svg|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			  loader: 'file-loader'
		    },
            {
                test: /\.scss$/,
                use: !process.env.PRED ? ExtractCss.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                minimize: true
                            }
                        }
                    ]
                }) : ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 400000,
                            outputPath: '/lib/images/'
                        }
                    }
                ]
            }
        ]
    },
}