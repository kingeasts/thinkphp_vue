const path = require('path')
const merge = require('webpack-merge')
const config = require('./common.js')
const webpack = require('webpack')
const ExtractCss = require('extract-text-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = merge(config, {
    resolve: {  //导入的时候不用写拓展名
        alias: {
            'vue': 'vue/dist/vue.min.js'
        }
    },
    plugins: [
        new ExtractCss('[name].css'),
        new CleanWebpackPlugin(['lib'], {
            root: process.env.PUBLIC_PATH,
            verbose: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'lib/vendor'
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: !process.env.PRED
        })
    ]
})