const webpack = require('webpack')
const path = require('path')

console.log('env', process.env.NODE_ENV)

process.env.PRED = process.env.NODE_ENV === 'development'
process.env.PUBLIC_PATH = path.join(__dirname, 'public')
process.env.RESOURCES_PATH = path.join(__dirname, 'resources')

// 打包到vendor.js的扩展
process.env.LibsExtends = ['vue', 'axios']

/*
const srcDir = path.resolve(process.cwd(), 'resources/')
const publicPath = path.join(__dirname, 'public')
const nodeModPath = path.resolve(__dirname, './node_modules')

let config = {
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        })
    ]
}
*/

module.exports = function () {
    return require(`./resources/webpack/${process.env.NODE_ENV}.js`)
}